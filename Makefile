# zde zmente na cestu k vasim nastrojum
saxon = ~/saxon9he.jar
fop = fop
jing = jing
browser = google-chrome

# promenne s cestami souboru
# zdrojove soubory

# spojeni
mergefile = src/merge.xml

# validace
dtd_schema = src/validate/schema.dtd
rnc_schema = src/validate/schema.rnc

# transformace
html_index_xsl = src/transform/html_index.xsl
html_countries_xsl = src/transform/html_countries.xsl
pdf_xsl = src/transform/pdf.xsl

# vysledne soubory
merged = build/count_merged.xml
pdf_output = build/countries.pdf


${merged}: # soubor se slitymi zememi
	@echo "merging based on ${mergefile} into ${merged}"
	@xmllint --dropdtd --noent --output ${merged} '${mergefile}' || (echo "merging failed $$?"; exit 1)
	@echo "success"

merge: ${merged} # pokud budeme chtit slevat samostatne

dtd: ${merged} # dtd validace
	@echo "validating ${merged} with ${dtd_schema}"
	@xmllint --noout --dtdvalid ${dtd_schema} ${merged} || (echo "dtd validation failed $$?"; exit 1)
	@echo "success"

rng: ${merged} # rng validace
	@echo "validating ${merged} with ${rnc_schema}"
	@${jing} -c ${rnc_schema} ${merged} || (echo "rnc validation failed $$?"; exit 1)
	@echo "success"

html: ${merged} # html generace
	@echo "generating html"
	@echo "generating index"
	@java -jar ${saxon} ${merged} ${html_index_xsl} || (echo "generating html index failed $$?"; exit 1)
	@echo "generating pages for each country"
	@java -jar ${saxon} ${merged} ${html_countries_xsl} || (echo "generating html pages for countries failed $$?"; exit 1)
	@echo "html has been generated into build/html directory"
	@echo "success"

pdf: ${merged}
	@echo "generating pdf"
	@${fop} -xml ${merged} -xsl ${pdf_xsl} -pdf ${pdf_output} || (echo "generating pdf failed $$?"; exit 1)
	@echo "pdf has been generated into build/countries.pdf"
	@echo "success"

clean: # vycisteni
	@echo "cleaning build/ directory"
	@rm -r build/*  || (echo "cleaning failed $$?"; exit 1)
	@echo "success"

open-pdf: pdf
	@echo "opening build/countries.pdf with ${browser}"
	@${browser} build/countries.pdf || (echo "opening pdf failed $$?"; exit 1)
	@echo "success"

open-html: html
	@echo "opening build/html/index.html with ${browser}"
	@${browser} build/html/index.html || (echo "opening html failed $$?"; exit 1)
	@echo "success"

present: html pdf
	@echo "opening README.md build/html/index.html and build/countries.pdf with ${browser}"
	@${browser} README.md || (echo "opening html failed $$?"; exit 1)
	@${browser} build/html/index.html || (echo "opening html failed $$?"; exit 1)
	@${browser} build/countries.pdf || (echo "opening pdf failed $$?"; exit 1)
	@echo "success"

help:
	@echo "available commands:"
	@echo "make merge"
	@echo "make dtd"
	@echo "make rng"
	@echo "make html"
	@echo "make pdf"
	@echo "make open-pdf"
	@echo "make open-html"
	@echo "make present"
	@echo "make clean"
# BI-XML Semestral project winter 2018/2019
Jakub Jun ( junjakub@fit.cvut.cz )

## Requirements

- [make](https://www.gnu.org/software/make/)
- [saxon](http://saxon.sourceforge.net/)
- [fop](https://xmlgraphics.apache.org/fop/)
- [jing](https://relaxng.org/jclark/jing.html)
- browser


## Configuration

Edit [Makefile](Makefile) and fill in paths of the binaries of the required tools mentioned above.


## How to run

Run following commands from the root of the repository.

- `make help` shows supported commands.
- `make merge` merges countries into one file
- `make dtd` validates against dtd schema
- `make rng` validates against rng schema
- `make html` generates html output
- `make pdf` generates pdf output
- `make present` opens html and pdf output in specified browser
- `make clean` to remove contents of `build/` directory


## File structure
```
├── build                 # contains output of make commands
│   ├── count_merged.xml  # merged countries into 1 xml file
│   ├── countries.pdf     # generated pdf file
│   └── html              # contains html generated output
│       ├── index.html
│       ├── Jamaica.html
│       ├── Japan.html
│       ├── Jersey.html
│       └── Jordan.html
├── docs                  # contains project specification
│   ├── BI-XML-PROJEKT-INSTRUKCE.txt
│   ├── BI-XML-PROJEKT-PREZENTACE
│   └── zadani_junjakub.xml
├── Makefile              # makes everything work!
├── README.md             # this file
└── src                   # contains source files
    ├── countries         # contains source xml for each country
    │   ├── Jamaica.gif   # flag
    │   ├── jamaica.xml
    │   ├── Japan.gif
    │   ├── japan.xml
    │   ├── Jersey.gif
    │   ├── jersey.xml
    │   ├── Jordan.gif
    │   └── jordan.xml
    ├── merge.xml         # merges countries into one file
    ├── transform         # contains transformation stylesheets
    │   ├── html_countries.xsl
    │   ├── html_index.xsl
    │   └── pdf.xsl
    └── validate          # contains validation schemas
        ├── schema.dtd
        └── schema.rnc
```
## RNC Validation examples

### Regular expression
```
...
element Energy {
        attribute name { xsd:NCName },
        element Electrification {
          attribute name { xsd:NCName },
          xsd:token { pattern = "[0-9]+.?[0-9]*%" }     # REGULARNI VYRAZ
        },
        element Electricity-production {
          attribute name { text },
          text
        }
      }
...
```

### nonNegativeInteger
```
element Transportation {
        attribute name { xsd:NCName },
        element Airports {
          attribute name { xsd:NCName },
          xsd:nonNegativeInteger
        },
        element Heliports {
          attribute name { xsd:NCName },
          xsd:nonNegativeInteger
        }?
      }
```

### String length
```
element Introduction {
        attribute name { xsd:NCName },
        element Background {
          attribute name { xsd:NCName },
          xsd:string { minLength="500" maxLength="2000" }   # OMEZENI DELKY, CHCEME ASPON 500 U KAZDEHO
        }
      }
```

## Sources
- https://www.cia.gov/library/publications/the-world-factbook/
- http://zvon.org/
- https://fit.cvut.cz/
- https://stackoverflow.com/
- https://gitlab.fit.cvut.cz/junjakub/bi-xml-semestral-project
- https://getbootstrap.com/docs/4.0/components
- https://www.w3schools.com/xml/xpath_intro.asp
- https://xmlgraphics.apache.org/fop/quickstartguide.html
- https://xmlgraphics.apache.org/fop/examples.html
- https://www.kosek.cz/xml/schema/rng.html
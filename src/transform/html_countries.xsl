<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output omit-xml-declaration='yes' indent='yes' />
  <xsl:template match="/">
    <xsl:apply-templates mode="pages" />
  </xsl:template>

  <xsl:template match="//location" mode="pages">
    <xsl:result-document href="build/html/{@name}.html">
      <html>

      <head>
        <title>Countries - BI-XML Project</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous" />
        <style>
          .container {
            margin-top: 50px;
          }
          .top {
            color: gray;
            float: right;
            font-size: 15px;
            margin-left: 10px;
          }
        </style>
      </head>

      <body>
        <main role="main" class="container">
          <h1 id="top">Countries</h1>
          <a href="index.html">Return</a>
          <div>
            <h2>
              <xsl:value-of select="@name" />
            </h2>
            <div class="main-section">
              <h3>List of contents:</h3>
                <xsl:apply-templates mode="contents" />
            </div>
            <xsl:apply-templates />
          </div>
        </main>
      </body>

      </html>
    </xsl:result-document>
  </xsl:template>

  <xsl:template match="/*/*/*[not(@src)]" mode="contents"> <!-- do rejstriku -->
    <li>
      <a href="#{@name}">
        <xsl:value-of select="@name" />
      </a>
    </li>
  </xsl:template>

  <xsl:template match="/*/*/*[not(@src)]"> <!-- hlavni sekce -->
    <h3 id="{@name}">
      <xsl:value-of select="@name" /><a class="top" href="#top">back to top</a>
    </h3>
    <p>
      <xsl:apply-templates />
    </p>
    <hr />
  </xsl:template>

  <xsl:template match="/*/*/*[@src]"> <!-- vlajka -->
    <h3>
      <xsl:value-of select="@name" />
    </h3>
    <p>
      <img style="width: 150px;" src="../../src/countries/{@src}" />
    </p>
  </xsl:template>

  <xsl:template match="/*/*/*//*"> <!-- podsekce -->
    <h4>
      <xsl:value-of select="@name" />
    </h4>
    <p>
      <xsl:value-of select="." />
    </p>
  </xsl:template>
</xsl:stylesheet>
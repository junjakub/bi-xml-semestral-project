<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output omit-xml-declaration='yes' indent='yes' />

  <xsl:template match="/">
    <xsl:result-document href="build/html/index.html">
      <html>

      <head>
        <title>Countries - BI-XML Project</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous" />
        <style>
          .container {
            margin-top: 50px;
          }
        </style>

      </head>

      <body>
        <main role="main" class="container">
          <h1>Countries</h1>
          <nav class="nav flex-column">
            <xsl:apply-templates mode="index" />
          </nav>
        </main>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js" integrity="sha384-zDnhMsjVZfS3hiP7oCBRmfjkQC4fzxVxFhBx8Hkz2aZX8gEvA/jsP3eXRCvzTofP"
          crossorigin="anonymous"></script>
      </body>

      </html>
    </xsl:result-document>
  </xsl:template>

  <xsl:template match="//location" mode="index">
    <li>
      <a class="nav-link" href="{@name}.html">
        <xsl:value-of select="@name" />
      </a>
    </li>
  </xsl:template>

</xsl:stylesheet>
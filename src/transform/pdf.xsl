<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output method="xml" indent="yes" />

  <xsl:template match="/">
    <fo:root>
      <fo:layout-master-set>
        <fo:simple-page-master master-name="A4-portrait" page-height="29.7cm" page-width="21.0cm" margin="2cm">
          <fo:region-body margin="1.5cm" />
          <fo:region-before extent="2cm" />
          <fo:region-after extent="2cm" display-align="after">
          </fo:region-after>
        </fo:simple-page-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="A4-portrait">
        <fo:flow flow-name="xsl-region-body" space-after="2cm">
          <fo:block font-size="26pt" font-weight="bold">Countries - BI-XML project</fo:block>
          <fo:block font-size="16pt" space-before="0.5cm">
            Jakub Jun
          </fo:block>
        </fo:flow>
      </fo:page-sequence>

      <fo:page-sequence master-reference="A4-portrait">
        <fo:static-content flow-name="xsl-region-after">
          <fo:block text-align="end">
            <fo:page-number />
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body" space-after="2cm">
          <fo:block font-size="20pt" space-before="0.5cm" font-weight="bold" page-break-before="always">
            List of contents:
          </fo:block>
          <xsl:apply-templates mode="content" />
        </fo:flow>
      </fo:page-sequence>

      <fo:page-sequence master-reference="A4-portrait">
        <fo:static-content flow-name="xsl-region-after">
          <fo:block text-align="end">
            <fo:page-number />
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <xsl:apply-templates mode="pages" />
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template match="//location" mode="content">
    <fo:block font-size="14pt" font-weight="bold" space-before="0.5cm">
      <fo:basic-link internal-destination="{@name}" text-decoration="underline">
        <xsl:value-of select="@name" />
      </fo:basic-link>
    </fo:block>
  </xsl:template>

  <xsl:template match="//location" mode="pages">
    <fo:block font-size="22pt" font-weight="bold" id="{@name}" page-break-before="always">
      <xsl:value-of select="@name" />
    </fo:block>
    <fo:block font-size="22pt" font-weight="bold">
      <xsl:apply-templates mode="content" />
    </fo:block>
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="/*/*/*[not(@src)]" mode="content">
    <fo:block font-size="12pt" font-weight="bold" space-before="0.5cm">
      <fo:basic-link internal-destination="{../@name}-{@name}" text-decoration="underline">
        <xsl:value-of select="@name" />
      </fo:basic-link>
    </fo:block>
  </xsl:template>

  <xsl:template match="/*/*/*[@src]">
    <fo:block font-size="18pt" font-weight="bold" space-before="30pt" id="{../@name}-{@name}">
      <xsl:value-of select="@name" />
    </fo:block>
    <fo:block>
      <fo:external-graphic src="file:src/countries/{@src}" content-height="10em" content-width="10em" />
    </fo:block>
  </xsl:template>

  <xsl:template match="/*/*/*[not(@src)]">
    <fo:block font-size="18pt" font-weight="bold" space-before="30pt" id="{../@name}-{@name}">
      <xsl:value-of select="@name" />
    </fo:block>
    <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="/*/*/*/*">
    <fo:block font-size="14pt" font-weight="bold" space-before="30pt">
      <xsl:value-of select="@name" />
    </fo:block>
    <fo:block text-indent="1cm">
      <xsl:value-of select="." />
    </fo:block>
  </xsl:template>

</xsl:stylesheet>